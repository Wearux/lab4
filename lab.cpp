#include "stdafx.h"
#include <stdio.h>
#include "mpi.h"
#include <math.h>


int main(int  argc, char * argv[])
{
	int rank, size, resultlen;
	double startwtime = 0.0, endwtime;
	char name[MPI_MAX_PROCESSOR_NAME];
	MPI_Status status;
	char *str = new char[255];
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(name, &resultlen);
	startwtime = MPI_Wtime();
	if (rank == 0) {
		str = "ACHTUNG!";
		MPI_Send(&str, 255, MPI_CHAR, 1, 0, MPI_COMM_WORLD);
		MPI_Recv(&str, 255, MPI_CHAR, size-1, 0, MPI_COMM_WORLD, &status);
		endwtime = MPI_Wtime();
		printf("Процесс %d получил сообщение %s от %d за %f секунд\n", rank, str, status.MPI_SOURCE, endwtime - startwtime);
		fflush(stdout);
	} 
	else {
			MPI_Recv(&str, 255, MPI_CHAR, rank-1, 0, MPI_COMM_WORLD, &status);
			endwtime = MPI_Wtime();
			printf("Процесс %d получил сообщение %s от %d за %f секунд\n", rank, str, status.MPI_SOURCE, endwtime - startwtime);
			fflush(stdout);
			MPI_Send(&str, 255, MPI_CHAR, (rank==size-1) ? 0 : rank+1, 0, MPI_COMM_WORLD);
	}
	endwtime = MPI_Wtime();
	if (rank == 0)
	printf("Общее время выполнения: %f \n", endwtime - startwtime);
	MPI_Finalize();
	return 0;

	

}